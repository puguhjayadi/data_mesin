<?php

date_default_timezone_set("Asia/Bangkok");


function connect_access($sql = ''){
	$dir_db 	= "D:/laragon/www/data_mesin/"; /*bisa diubah*/
	$db 		= 'DataStoreADB.accdb'; /*Hotmix*/
	$DBname 	= $dir_db.''.$db;

	if (!file_exists($DBname)) {
		echo "File Access tidak ditemukan";
	} else if ($sql != '') {
		$driver = "{Microsoft Access Driver (*.mdb, *.accdb)}";
		$db     = new PDO("odbc:Driver=".$driver."; Dbq=$DBname; Uid=; Pwd=;");
		return $db->query($sql);
	}
}

function read_json(){
	$dir_json 	= "D:/laragon/www/data_mesin/";
	$getfile 	= file_get_contents($dir_json.'last_data_hotmix.json');
	return json_decode($getfile);
}

function update_json($last_date_send, $last_time_send){
	$dir_json 	= "D:/laragon/www/data_mesin/";
	
	$last_data = array('last_date' => $last_date_send, 'last_time' => $last_time_send);
	$newString = json_encode($last_data);
	file_put_contents($dir_json.'last_data_hotmix.json', $newString);
	return true;
}

function send_curl($data){
	$url 			= 'http://localhost/adp6/post_dm_hotmix.php'; /* url post */
	$field_string 	= http_build_query($data);

	$ch = curl_init();
	curl_setopt($ch,CURLOPT_URL, $url);
	curl_setopt($ch,CURLOPT_POST, 1);
	curl_setopt($ch,CURLOPT_POSTFIELDS, $field_string);

	$result = curl_exec($ch);
	curl_close($ch);
	return true;
}

function is_connected(){
	$connected = @fsockopen("www.google.com", 80);  /* cek koneksi */
	if ($connected){
		fclose($connected);
		$msg = 1;
	} else{
		$msg = 0;
	}
	return $msg;
}


if (is_connected() == 1){
	connect_access();
	$jsonfile 	= read_json();
	$last_date  = $jsonfile->last_date;
	$last_time  = $jsonfile->last_time;
	$sql_cek 	= "SELECT MAX(ProdDate) as max_date FROM READRECORD";
	$result_cek = connect_access($sql_cek)->fetch();

	$max_date   = substr($result_cek['max_date'], 0, 10);	/* ambil tanggal terakhir dari Data Mesin*/

	if($max_date == $last_date){
		$last_date 	= "#".$last_date."#";
		$sql  		= "SELECT TOP 10 * FROM READRECORD WHERE DateValue(ProdDate) = $last_date AND TimeValue(TIME) > TimeValue('$last_time') ORDER BY ProdDate DESC, TIME ASC";
	} else {
		$last_date 	= "#".$last_date."#";
		$sql  		= "SELECT TOP 10 * FROM READRECORD WHERE DateValue(ProdDate) > $last_date AND TimeValue(TIME) > TimeValue('$last_time') ORDER BY ProdDate DESC, TIME ASC";
	}


	$result = connect_access($sql);

	while ($row = $result->fetch()) {
		for ($i=0; $i < 33; $i++) { 
			unset($row[$i]); /* remove index 0,1,2,... */
		}
		$datane[] = $row;
	}

	$last_key 		= end($datane);
	$last_date_send = substr($last_key['ProdDate'], 0, 10);
	$last_time_send = substr($last_key['TIME'], 11, 20);

	send_curl($datane);
	update_json($last_date_send, $last_time_send);
} else {
	echo "Tidak tersambung internet";
}
?>